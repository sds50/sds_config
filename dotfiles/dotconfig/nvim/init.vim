
" auto-install vim-plug
if has('nvim')
    if empty(glob('~/.config/nvim/autoload/plug.vim'))
        silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall
    endif
else
    if empty(glob('~/.vim/autoload/plug.vim'))
        silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall
    endif
endif
call plug#begin('~/.config/nvim/plugged')

" List plugins
Plug 'tpope/vim-sensible'
Plug 'scrooloose/nerdtree'               " See \E
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'scrooloose/nerdcommenter'          " See \cc, \c<space> and <C-/>
Plug 'ctrlpvim/ctrlp.vim'                " Fuzzy file finding. To set base dir, use :CtrlP <dir>
Plug 'jlanzarotta/bufexplorer'
Plug 'vim-syntastic/syntastic'           " Syntax erros while typing
Plug 'sodapopcan/vim-twiggy'             " :Twiggy gives git functionality
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'rodjek/vim-puppet'

" For neovim support
Plug 'beeender/Comrade'

" Deoplete
if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
    Plug 'Shougo/deoplete.nvim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

Plug 'zchee/deoplete-clang'
let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-10/lib/libclang.so.1'
let g:deoplete#sources#clang#clang_header = '/usr/lib/llvm-10/lib/clang/10.0.0/include/'
let g:python3_host_prog = '$HOME/local/pkg/miniconda3/bin/python'

" Plugins visible to vim after this call
call plug#end()

" Reset colours on leaving vim
au VimLeave * silent! !echo -ne "\033[0m"

" Disable entering Ex mode. It is far too irritating.
map Q <Nop>

" Tab options
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smarttab
set cindent

" Search options
set ignorecase
set smartcase
set hlsearch
set incsearch

" Enable nice option completion
set wildmenu
set wildmode=list:longest

" Line numbers by default
set number

" Allow us to have unsaved buffers in the background without !-confirmation.
set hidden
set confirm

" Misc
set encoding=utf-8
set modelines=0

" A quick access toggle for NERDTree
nmap <silent> <Leader>E <Esc>:NERDTreeFind<cr>
" au FileType nerdtree nmap <silent> <Leader>E <Esc>:NERDTreeToggle<cr>
let NERDTreeQuitOnOpen=1
au FileType nerdtree nmap <buffer> <left> x
au FileType nerdtree nmap <buffer> <right> o
