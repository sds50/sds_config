#!/usr/bin/env python3

from __future__ import absolute_import
import tempfile
import subprocess
import sys
import os
import re


def conditional_link(src, tgt):
    """
    Links a source file/dir to the target if the link doesn't already exist.

    If the file/link exists and is already pointing to the right place, it is
    skipped, otherwise this is noted.
    """
    # Ensure that the target directory exists
    if not os.path.exists(os.path.dirname(tgt)):
        os.makedirs(os.path.dirname(tgt))

    if os.path.isfile(tgt) or os.path.isdir(tgt):
        if os.path.islink(tgt) and os.path.realpath(src) == os.path.realpath(tgt):
            pass
            # sys.stdout.write("(exists)")
        else:
            sys.stdout.write("\033[91m(DIFFERS)\033[00m")
    else:
        # If the file is now a broken link (i.e. we have moved the target dotfile) then fix it
        if os.path.lexists(tgt) and not os.path.exists(tgt):
            os.remove(tgt)
        os.symlink(src, tgt)
        sys.stdout.write("\033[92m(new)\033[00m")


def iterate_files(src_dir, excludes=None):
    """
    Iterate over the _files_ to consider in a given directory
    """
    def dotty(path):
        return '/'.join([".{}".format(p[3:]) if p[0:3] == 'dot' else p for p in path.split('/')])

    for root, dirs, files in os.walk(src_dir):

        relative_root = os.path.relpath(root, src_dir)

        for f in files:

            # We link visible files only. Skip any actual dotfiles.
            if f[0] == '.':
                continue

            dest_path = dotty(os.path.join(relative_root, f))
            if dest_path[0:2] == './':
                dest_path = dest_path[2:]

            yield dest_path, os.path.join(root, f)


def install_dotfiles(config_dir):

    dotfile_dir = os.path.join(config_dir, "dotfiles")

    print()
    print("Installing dotfiles...")
    print("Source dir: {}".format(dotfile_dir))

    for name, path in iterate_files(dotfile_dir):

        sys.stdout.write(" > ~/{} ".format(name))
        target_name = os.path.expanduser('~/{}'.format(name))
        conditional_link(path, target_name)
        sys.stdout.write("\n")





def install_local_bin(src_dir):

    # If the binary directory doesn't exist, then create it.
    bin_tgt_dir = os.path.expanduser("~/local/bin")
    if not os.path.exists(bin_tgt_dir):
        print()
        print("Creating directory: {}".format(bin_tgt_dir))
        os.makedirs(bin_tgt_dir)

    bin_dir = os.path.join(config_dir, u'local_bin')

    print()
    print("Installing executables and scripts...")
    print("Source dir: {}".format(bin_dir))

    # For executables, we only want to link properly named commands. Exclude .py files.
    for name, path in iterate_files(bin_dir, [re.compile(".*\.py$")]):

        sys.stdout.write(" > ~/local/bin/{} ".format(name))
        target_name = os.path.join(bin_tgt_dir, name)
        conditional_link(path, target_name)
        sys.stdout.write("\n")
#
#
#def git_settings():
#
#    # Update git settings
#
#    def config_git_new(param, default):
#        git_str = [u'git', u'config', u'--global']
#        try:
#            existing = subprocess.check_output(git_str + [param]).decode().splitlines()[0]
#        except subprocess.CalledProcessError:
#            subprocess.check_call(git_str + [param, default])
#            return default
#        return existing
#
#    print
#    print "Updating git global configuration..."
#    print " > Exclude file: {}".format(
#        config_git_new(u'core.excludesfiles', os.path.expanduser(u'~/.gitignore')))
#    print " > User name: {}".format(
#        config_git_new(u'user.name', u'Simon Smart'))
#    print " > Email: {}".format(
#        config_git_new(u'user.email', u'simondsmart@gmail.com'))
#
#
#def vim_config():
#
#    # Ensure that vundle and the .vim directory exist
#
#    bundle_dir = os.path.expanduser("~/.vim/bundle")
#    if not os.path.exists(bundle_dir):
#        os.makedirs(bundle_dir)
#
#    # TODO: Use Repo abstraction
#    vundle_dir = os.path.join(bundle_dir, "Vundle.vim")
#    if not os.path.exists(vundle_dir):
#        print
#        print "Installing Vundle for vim management"
#        subprocess.check_call([u'git', u'clone', "https://github.com/VundleVim/Vundle.vim.git",
#                               vundle_dir], stdout=sys.stdout)
#
#    print
#    print "Updating vim plugins..."
#    # When not run from a terminal, vim damages the terminal (requiring a reset) if stderr still
#    # finds its way to the terminal, so ensure that it doesn't
#    with open(os.devnull, 'w') as fnull:
#        subprocess.check_call([u'vim', u'+PluginInstall', u'+qall'], stderr=fnull)
#
#
#def python_libs(src_dir):
#
#    python_lib_src = os.path.join(src_dir, "python")
#    python_lib_tgt = os.path.expanduser("~/.local/lib/python-packages")
#
#    # If the binary directory doesn't exist, then create it.
#    lib_tgt_dir = os.path.expanduser("~/.local/lib")
#    if not os.path.exists(lib_tgt_dir):
#        print
#        print "Creating directory: {}".format(lib_tgt_dir)
#        os.makedirs(lib_tgt_dir)
#
#    print
#    print "Linking python packages"
#    sys.stdout.write(" > {} ".format(python_lib_tgt))
#    conditional_link(python_lib_src, python_lib_tgt)


if __name__ == u'__main__':

    print("Updating local configuration...")

    config_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    print("Configuration directory: {}".format(config_dir))

#    python_libs(config_dir)
#
    install_dotfiles(config_dir)

    # Ensure that we have appropriate keys
    # ...

    # Ensure_permissions (.ssh directory should only be readable to user)

    install_local_bin(config_dir)
#
#    git_settings()
#
#    vim_config()

# Python modules, and scripts, from another repo???

# Link the modules in python to ~/local/python

# Install vim modules!!!
