import collections


def update_nested(target_dict, source_dict):
    """
    Update a target dict with elements from a source dict. If the element from the source a dictionary like
    object, then update only using its elements, rather than replacing the whole mapping.

    i.e.

    tgt = {
       "a": {
          "b": 1,
          "c": 2
       }
    }

    src = {
       "a": {
          "b": 3
       }
    }

    update_nested(tgt, src) results in:

    tgt = {
       "a": {
          "b": 3,
          "c": 2
       }
    }

    :param target_dict: The dictionary to be modified IN PLACE
    :param source_dict: The dictionary to source elements from
    :return: The reference to target_dict
    """
    for k, v in source_dict.iteritems():
        if isinstance(v, collections.Mapping):
            r = update_nested(target_dict.get(k, {}), v)
            target_dict[k] = r
        else:
            target_dict[k] = source_dict[k]
    return target_dict
