import os
import re
import errno
import subprocess

class Repo(object):

    def __init__(self, local_path, remote_uri):
        """
        Represent a (git) repo. If the repo has never been cloned, then set that up. Otherwise
        if something is there (but it isn't git...) then bail out
        """
        self.local_path = local_path
        self.remote_uri = remote_uri

    def git(self, *args):
        """
        Execute a git command with the specified arguments
        """
        cwd = os.getcwd()
        if args[0] != 'clone':
            os.chdir(self.local_path)
        output = ""
        try:
            output = subprocess.check_output(['git'] + list(args), stderr=subprocess.PIPE).strip()
        finally:
            os.chdir(cwd)
        return output

    def fetch(self):
        """
        Fetch from the remote, to ensure we know what the remote state is
        """
        self.git('fetch')

    def clone(self):
        """
        Clone this repo!
        """
        # We might end up calling clone on a repository that already exists if we are doing
        # so as part of a recursive stack. Just be silent.
        if not self.initialised:

            if os.path.exists(self.local_path):
                raise Exception("Directory {} already exists, but is not repo".format(self.local_path))

            # Ensure that the path to clone into exists
            parent_path = os.path.dirname(self.local_path)
            try:
                os.makedirs(parent_path)
            except OSError as e:
                if e.errno == errno.EEXIST and os.path.isdir(parent_path):
                    pass
                else:
                    raise

            self.git('clone', self.remote_uri, self.local_path)

    def merge(self, origin='origin', branch_name=None):
        """
        Perform "git merge origin/branch". It always requires the tracked branch to have the same name. Anything more
        complex should be done manually (for now, at least)

        :param origin: If there is an origin other than origin, use that
        :param branch_name: If this is specified, switch to branch branch_name, then do the merge on that branch,
                            and then switch back.
        """
        branch_tmp = self.branch
        if branch_name:
            self.branch = branch_name

        output = self.git('merge', '{}/{}'.format(origin, self.branch)).decode()

        if branch_name:
            self.branch = branch_tmp

        return output

    @property
    def clean(self):
        return len(self.git('status', '--porcelain')) == 0

    @property
    def initialised(self):
        """
        If git-status fails, it is due to the path not being an initialised repo
        """
        if not (os.path.exists(self.local_path) and os.path.isdir(self.local_path)):
            return False
        try:
            self.git('status', '--porcelain')
        except subprocess.CalledProcessError as e:
            return False
        return True

    @property
    def branches(self):

        re_branch = re.compile("^[ \*]*([^ \(]+|\(detached from [a-f0-9]{7}\)) *([a-f0-9]{7}) *(\[(behind|ahead) ([0-9]+)\])?(.*)$")
        class Branch:
            def __init__(self, git_branch_out):
                m = re_branch.match(git_branch_out)
                self.name = m.groups()[0]
                self.shaid = m.groups()[1]
                self.behind = (m.groups()[2] != None) and (m.groups()[3] == 'behind')
                self.ahead = (m.groups()[2] != None) and (m.groups()[3] == 'ahead')
                self.divergence = int(m.groups()[4]) if (m.groups()[2] != None) else 0

            @property
            def divergence_string(self):
                return "{} commit{}".format(self.divergence, "s" if self.divergence > 1 else "")

        return [Branch(br) for br in self.git('branch', '-v').decode().splitlines()]

    @property
    def branch(self):
        return self.git('rev-parse', '--symbolic-full-name', '--abbrev-ref', 'HEAD').decode()

    @branch.setter
    def branch(self, new_branch):
        if self.branch != new_branch:
            self.git('checkout', new_branch)

    @property
    def shaid(self):
        return self.git('rev-parse', 'HEAD')

