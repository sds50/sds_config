#!/usr/bin/env python3

import subprocess
import tempfile
import os

from util.print_colour import print_colour
from util.repo import Repo
from urllib.request import urlretrieve

system_packages = """
    aptitude
    tmux
    git
    gitk
    curl
    neovim
    build-essential
    liblapack-dev
    libopenmpi-dev
    libhdf5-openmpi-dev
    gfortran
    clang
    ack
    ninja-build
    sshoot
    cmake
    ninja-build
"""
system_packages = [p.strip() for p in system_packages.split('\n') if p.strip() != '']


# TODO: Abstract repo management somewhere else central
git_repos = [
    ('~/git/config/liquidprompt', 'https://github.com/nojhan/liquidprompt.git')
]


def run_sudo(*args):
    print("  > sudo {}".format(' '.join(args)))
    subprocess.check_call(['sudo'] + list(args), stdout=subprocess.DEVNULL)


def run_self(*args):
    print("  > {}".format(' '.join(args)))
    subprocess.check_call(list(args), stdout=subprocess.DEVNULL)


def fullpath(pth):
    # n.b. not os.path.realpath. Symlinks are useful.
    return os.path.abspath(os.path.expanduser(os.path.expandvars(pth)))


def install_google_chrome():

    print_colour('cyan', 'Installing Google Chrome ...')

    with tempfile.TemporaryDirectory() as d:

        fn = 'google-chrome-stable_current_amd64.deb'
        installer = os.path.join(d, fn)

        urlretrieve('https://dl.google.com/linux/direct/{}'.format(fn), installer)

        run_sudo('dpkg', '-i', installer)
        run_sudo('apt-get', 'install', '-f', '-y')


def install_miniconda():

    with tempfile.TemporaryDirectory() as d:

        fn = 'Miniconda3-latest-Linux-x86_64.sh'
        installer = os.path.join(d, fn)

        urlretrieve("https://repo.continuum.io/miniconda/{}".format(fn), installer)

        os.chmod(installer, 0o755)
        run_self(installer, '-b', '-p', fullpath('~/local/pkg/miniconda3'))
        

#def install_qtcreator():
#
#    print_colour('cyan', 'Installing qtcreator')
#
#    with tempfile.TemporaryDirectory() as d:
#
#        fn = 'qt-unified-linux-x64-online.run'
#        installer = os.path.join(d, fn)
#
#        urlretrieve('https://download.qt.io/official_releases/online_installers/{}'.format(fn), installer)
#
#        os.chmod(installer, 0o755)
#        subprocess.check_call([installer])


if __name__ == '__main__':

    print_colour('cyan', 'Updating package cache ...')
    run_sudo('apt-get', 'update')

    print_colour('cyan', 'General update ...')
    #run_sudo('apt-get', '-y', 'upgrade')

    #for pkg in system_packages:
    #    print_colour('cyan', 'Checking package {} ...'.format(pkg))
    #    try:
    #        run_sudo('dpkg', '-s', pkg)
    #    except subprocess.CalledProcessError:
    #        print_colour('green', '  > not found. installing ...')
    #        run_sudo('apt-get', '-y', 'install', pkg)
    #        print_colour('green', '  > done')

    #for pth, repo in git_repos:
    #    print_colour('cyan', 'Checking repo: {} ({})'.format(pth, repo))
    #    r = Repo(os.path.expanduser(os.path.expandvars(pth)), repo)
    #    if not os.path.exists(pth):
    #        print('  > Cloning')
    #        r.clone()
    #    else:
    #        r.fetch()
    #        r.merge()

    #install_google_chrome()
    #install_qtcreator()
    install_miniconda()
