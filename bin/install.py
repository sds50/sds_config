#!/usr/bin/env python3
"""
Install the git hooks required to keep the config up to date.
"""

from __future__ import absolute_import
import os
import subprocess


def force_symlink(src, tgt):
    """
    Create a symlink, even if that means overwriting it.
    """
    try:
        os.symlink(src, tgt)
    except OSError:
        os.remove(tgt)
        os.symlink(src, tgt)


if __name__ == '__main__':

    bin_dir = os.path.dirname(os.path.realpath(__file__))
    hook_dir = os.path.join(os.path.dirname(bin_dir), ".git/hooks")
    update_script = os.path.join(bin_dir, 'update.py')

    print("Installing git hooks into: {}".format(hook_dir))

    force_symlink(update_script, os.path.join(hook_dir, 'post-merge'))
    force_symlink(update_script, os.path.join(hook_dir, 'post-commit'))

    print("Running initial update")
    subprocess.call([update_script])

