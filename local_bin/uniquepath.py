#!/usr/bin/env python3

import os
import sys

assert len(sys.argv) in (1, 2)
envvar = 'PATH' if len(sys.argv) == 1 else sys.argv[1]

pathset = set()
paths = []

for path in os.environ[envvar].split(':'):
    rpath = os.path.abspath(os.path.realpath(path))
    if rpath not in pathset:
        pathset.add(rpath)
        paths.append(rpath)

print("export {}={}".format(envvar, ':'.join(paths)))
