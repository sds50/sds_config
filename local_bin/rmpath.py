#!/usr/bin/env python3

import os
import sys

assert len(sys.argv) in (2, 3)
envvar = 'PATH' if len(sys.argv) == 2 else sys.argv[2]

newpath = os.path.abspath(os.path.realpath(sys.argv[1]))

oldpaths = [os.path.abspath(os.path.realpath(p)) for p in os.environ[envvar].split(':')]
paths = [p for p in oldpaths if p != newpath]

print("export {}={}".format(envvar, ':'.join(paths)))
