#!/usr/bin/env python3
import os
import getpass

sw = ['git', 'build']
user = getpass.getuser()
home_paths = [p.format(user) for p in ('/var/tmp/{}/', '/perm/ma/{}/', '/lus/snx11062/scratch/ma/{}/')]

cwd = os.getcwd()
home = os.path.expanduser('~')

for p in home_paths:
    if cwd.startswith(p):
        cwd = os.path.join(home, os.path.relpath(cwd, p))

cmd = "true"
for src, tgt in zip(sw, reversed(sw)):
    if cwd.startswith(os.path.join(home, src)):
        cmd = "cd {}".format(os.path.join(home, tgt, os.path.relpath(cwd, os.path.join(home, src))))

print(cmd)
